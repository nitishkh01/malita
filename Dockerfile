FROM node:8.9.4

WORKDIR /profiling-vue

COPY package.json /profiling-vue

COPY . /profiling-vue

RUN npm install

ENV HOST 0.0.0.0