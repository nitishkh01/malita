// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import App from './App'
import router from './router'
import Vue from 'vue'
import Vuex from 'vuex'

// 3rd party library
import 'chart.js'
import 'hchs-vue-charts'
import 'vue-scrollto'

import 'material-design-icons'

import infiniteScroll from 'vue-infinite-scroll'

// Import store
import store from './store'

import 'vue-croppa/dist/vue-croppa.css'
import Croppa from 'vue-croppa'

import {
  VAlert,
  VApp,
  VAvatar,
  VBtn,
  VCard,
  VCheckbox,
  VChip,
  VDataTable,
  VDialog,
  VDivider,
  VExpansionPanel,
  VFooter,
  VGrid,
  VIcon,
  VList,
  VNavigationDrawer,
  VPagination,
  VProgressCircular,
  VSelect,
  VSnackbar,
  VSpeedDial,
  VStepper,
  VTabs,
  VTextField,
  VToolbar,
  VTooltip,
  Vuetify,
  VSwitch,
  transitions
} from 'vuetify'

Vue.use(Vuetify, {
  components: {
    VAlert,
    VApp,
    VAvatar,
    VBtn,
    VCard,
    VCheckbox,
    VChip,
    VDataTable,
    VDialog,
    VDivider,
    VExpansionPanel,
    VFooter,
    VGrid,
    VIcon,
    VList,
    VNavigationDrawer,
    VPagination,
    VProgressCircular,
    VSelect,
    VSpeedDial,
    VSnackbar,
    VStepper,
    VTabs,
    VTextField,
    VToolbar,
    VTooltip,
    VSwitch,
    transitions
  }
})

Vue.use(window.VueCharts)
Vue.use(Vuex)
Vue.use(infiniteScroll)
Vue.use(Croppa)

Vue.filter('capitalize', function (value, maxLen) {
  const len = maxLen || 0
  if (value.length <= len) {
    return value.toUpperCase()
  }
  return value ? value.charAt(0).toUpperCase() + value.slice(1) : ''
})

Vue.config.productionTip = false
Vue.config.debug = false

Vue.prototype.$eventHub = new Vue()  // Global event bus

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  template: '<App/>',
  components: { App }
})
