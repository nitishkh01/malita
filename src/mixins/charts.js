import FiltersMixin from './filters'

export default {
  methods: {
    capitalizeLabels: function (objList, defaultValue) {
      if (!defaultValue) {
        defaultValue = 'None'
      }
      return objList.map(value => {
        return value ? value.charAt(0).toUpperCase() + value.slice(1) : defaultValue
      })
    },
    getValuesByKey: function (objList, key) {
      if (objList.length > 0) {
        return objList.map((item) => item[key])
      } else {
        return []
      }
    }
  },
  mixins: [FiltersMixin]
}
