export default {
  computed: {
    storeFilters: function () {
      return this.$store.getters['filters/filters']
    },
    storeChoices: function () {
      return this.$store.getters['families/constants']
    }
  },
  methods: {
    getEmptyValue: function (key) {
      return Array.isArray(this.filterModels[key]) ? [] : ''
    },
    loadStoreFilter: function (key) {
      // Converts filterModels to `storeFilters`
      return Array.isArray(this.filterModels[key]) ? this.filterModels[key].join(',') : this.filterModels[key]
    },
    parseStoreFilter: function (key) {
      // Converts store filter to `filterModels`
      return Array.isArray(this.filterModels[key]) ? this.filterToArray(key) : this.filterModels[key]
    },
    filterToArray: function (key) {
      return this.storeFilters[key].split(',').filter(value => value !== '')
    },
    redirectToHome: function () {
      this.$router.push({name: 'home-page'})
    },
    setFilterModels: function () {
      Object.keys(this.filterModels).forEach((key) => {
        this.$set(this.filterModels, key, this.parseStoreFilter(key))
      })
    },
    resetFilterModels: function () {
      Object.keys(this.filterModels).forEach((key) => {
        this.$set(this.filterModels, key, this.getEmptyValue(key))
      })
    }
  }
}
