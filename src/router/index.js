import Vue from 'vue'
import Router from 'vue-router'

// This will lazy load the component, it means the component will only be loaded if it is used
const HomePage = () => import(/* webpackChunkName: "home-page" */ '@/pages/HomePage')
const ProfilePage = () => import(/* webpackChunkName: "profile-page" */ '@/pages/ProfilePage')

Vue.use(Router)

export default new Router({
  mode: 'history',  // This will remove the `#/` in the url
  routes: [
    {
      path: '/',
      name: 'home-page',
      component: HomePage
    },
    {
      path: '/profile/:familyId',
      name: 'profile-page',
      component: ProfilePage,
      props: true
    }
  ]
})
