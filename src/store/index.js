import Vue from 'vue'
import Vuex from 'vuex'

import education from './modules/education'
import expenditures from './modules/expenditures'
import families from './modules/families'
import filters from './modules/filters'
import health from './modules/health'
import housing from './modules/housing'
import livestock from './modules/livestock'
import psychosocial from './modules/psychosocial'
import statistics from './modules/statistics'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    education,
    expenditures,
    families,
    filters,
    health,
    housing,
    livestock,
    psychosocial,
    statistics
  }
})

export default store
