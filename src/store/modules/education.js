import { Http } from '@/http'

const state = {
  education: [],
  postEducation: [],
  patchEducation: []
}

const getters = {
  education: state => state.education,
  postEducation: state => state.postEducation,
  patchEducation: state => state.patchEducation
}

const mutations = {
  setEducation: (state, education) => {
    state.education = education
  },
  setPatchEducation: (state, patchEducation) => {
    state.patchEducation = patchEducation
  },
  setPostEducation: (state, postEducation) => {
    state.postEducation = postEducation
  }
}

const actions = {
  fetchEducation: ({ commit }, id) => {
    let url = `/education-profiles/${id}`

    return new Promise((resolve, reject) => {
      Http.get(url)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error.response)
          })
    })
  },
  patchEducation: ({ commit }, payload) => {
    let url = `/education-profiles/${payload.id}/`
    return new Promise((resolve, reject) => {
      Http.patch(url, payload)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            console.log(error.response)
            reject(error.response)
          })
    })
  },
  postEducation: ({ commit }, payload) => {
    let url = `/education-profiles/`
    return new Promise((resolve, reject) => {
      Http.post(url, payload)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            console.log(error.response)
            reject(error.response)
          })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
