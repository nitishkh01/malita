import { Http } from '@/http'

const state = {
  expenditures: [],
  patchExpenditures: []
}

const getters = {
  expenditures: state => state.expenditures,
  patchExpenditures: state => state.patchExpenditures
}

const mutations = {
  setExpenditures: (state, expenditures) => {
    state.expenditures = expenditures
  },
  setPatchExpenditures: (state, patchExpenditures) => {
    state.patchExpenditures = patchExpenditures
  }
}

const actions = {
  fetchExpenditures: ({ commit }, id) => {
    let url = `/expenditures/${id}`

    return new Promise((resolve, reject) => {
      Http.get(url)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error.response)
          })
    })
  },
  patchExpenditures: ({ commit }, payload) => {
    let url = `/expenditures/${payload.id}/`
    return new Promise((resolve, reject) => {
      Http.patch(url, payload)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            console.log(error.response)
            reject(error.response)
          })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
