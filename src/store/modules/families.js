import Vue from 'vue'
import querystring from 'querystring'

import { Http } from '@/http'

const state = {
  barangays: [],
  constants: {
    gender: [],
    civil_status: [],
    religion: [],
    blood_type: [],
    yes_no: []
  },
  families: [],
  family: {
    head: {},
    spouse: {}
  },
  familyAssets: [],
  familyIncome: [],
  patchFamily: [],
  patchFamilyHead: [],
  patchFamilySpouse: [],
  patchFamilyIncome: [],
  patchFamilyAssets: []
}

const getters = {
  barangays: state => state.barangays,
  constants: state => state.constants,
  families: state => state.families,
  family: state => state.family,
  familyAssets: state => state.familyAssets,
  familyIncome: state => state.familyIncome,
  patchFamily: state => state.patchFamily,
  patchFamilyHead: state => state.patchFamilyHead,
  patchFamilySpouse: state => state.patchFamilySpouse,
  patchFamilyIncome: state => state.patchFamilyIncome,
  patchFamilyAssets: state => state.patchFamilyAssets
}

const mutations = {
  setFamilyHeadImage: (state, imageUrl) => {
    Vue.set(state.family.head, 'image', imageUrl)
  },
  setBarangays: (state, barangays) => {
    state.barangays = barangays
  },
  setConstants: (state, constants) => {
    state.constants = constants
  },
  setFamilies: (state, families) => {
    state.families = families
  },
  setFamily: (state, family) => {
    state.family = family
  },
  setFamilyAssets: (state, familyAssets) => {
    state.familyAssets = familyAssets
  },
  setFamilyIncome: (state, familyIncome) => {
    state.familyIncome = familyIncome
  },
  setPatchFamily: (state, patchFamily) => {
    state.patchFamily = patchFamily
  },
  setPatchFamilyHead: (state, patchFamilyHead) => {
    state.patchFamilyHead = patchFamilyHead
  },
  setPatchFamilySpouse: (state, patchFamilySpouse) => {
    state.patchFamilySpouse = patchFamilySpouse
  },
  setPatchFamilyIncome: (state, patchFamilyIncome) => {
    state.patchFamilyIncome = patchFamilyIncome
  },
  setPatchFamilyAssets: (state, patchFamilyAssets) => {
    state.patchFamilyAssets = patchFamilyAssets
  }
}

const actions = {
  fetchBarangays: ({ commit }, filters) => {
    let url = `/families/barangays/`

    if (filters) {
      url = `${url}?${querystring.stringify(filters)}`
    }
    return new Promise((resolve, reject) => {
      Http.get(url)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error.response)
          })
    })
  },
  fetchConstants: ({ commit }, filters) => {
    let url = `/families/constants/`

    if (filters) {
      url = `${url}?${querystring.stringify(filters)}`
    }
    return new Promise((resolve, reject) => {
      Http.get(url)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error.response)
          })
    })
  },
  fetchFamilies: ({ commit }, filters) => {
    let url = `/families/`

    if (filters) {
      url = `${url}?${querystring.stringify(filters)}`
    }
    return new Promise((resolve, reject) => {
      Http.get(url)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error.response)
          })
    })
  },
  fetchFamily: ({ commit }, id) => {
    let url = `/families/${id}/`
    return new Promise((resolve, reject) => {
      Http.get(url)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error.response)
          })
    })
  },
  fetchFamilyAssets: ({ commit }, id) => {
    let url = `/assets/${id}/`
    return new Promise((resolve, reject) => {
      Http.get(url)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error.response)
          })
    })
  },
  fetchFamilyIncome: ({ commit }, id) => {
    let url = `/income/${id}/`
    return new Promise((resolve, reject) => {
      Http.get(url)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error.response)
          })
    })
  },
  patchFamily: ({ commit }, payload) => {
    let url = `/families/${payload.id}/`
    return new Promise((resolve, reject) => {
      Http.patch(url, payload)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            console.log(error.response)
            reject(error.response)
          })
    })
  },
  patchFamilyHead: ({commit}, payload) => {
    let url = `/families/heads/${payload.id}/`
    return new Promise((resolve, reject) => {
      Http.patch(url, payload)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            console.log(error.response)
            reject(error.response)
          })
    })
  },
  patchFamilySpouse: ({commit}, payload) => {
    let url = `/families/spouses/${payload.id}/`
    return new Promise((resolve, reject) => {
      Http.patch(url, payload)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error.response)
          })
    })
  },
  patchFamilyIncome: ({commit}, payload) => {
    let url = `/income/${payload.id}/`
    return new Promise((resolve, reject) => {
      Http.patch(url, payload)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error.response)
          })
    })
  },
  patchFamilyAssets: ({commit}, payload) => {
    let url = `/assets/${payload.id}/`
    return new Promise((resolve, reject) => {
      Http.patch(url, payload)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error.response)
          })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
