import { Http } from '@/http'

const state = {
  health: [],
  patchHealth: []
}

const getters = {
  health: state => state.health,
  patchHealth: state => state.patchHealth
}

const mutations = {
  setHealth: (state, health) => {
    state.health = health
  },
  setPatchHealth: (state, patchHealth) => {
    state.patchHealth = patchHealth
  }
}

const actions = {
  fetchHealth: ({ commit }, id) => {
    let url = `/nutrition-profiles/${id}`

    return new Promise((resolve, reject) => {
      Http.get(url)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error.response)
          })
    })
  },
  patchHealth: ({ commit }, payload) => {
    let url = `/nutrition-profiles/${payload.id}/`
    return new Promise((resolve, reject) => {
      Http.patch(url, payload)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            console.log(error.response)
            reject(error.response)
          })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
