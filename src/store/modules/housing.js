import { Http } from '@/http'

const state = {
  housing: [],
  patchHousing: []
}

const getters = {
  housing: state => state.housing,
  patchHousing: state => state.patchHousing
}

const mutations = {
  setHousing: (state, housing) => {
    state.housing = housing
  },
  setPatchHousing: (state, patchHousing) => {
    state.patchHousing = patchHousing
  }
}

const actions = {
  fetchHousing: ({ commit }, id) => {
    let url = `/housing/${id}`

    return new Promise((resolve, reject) => {
      Http.get(url)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error.response)
          })
    })
  },
  patchHousing: ({ commit }, payload) => {
    let url = `/housing/${payload.id}/`
    return new Promise((resolve, reject) => {
      Http.put(url, payload)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            console.log(error.response)
            reject(error.response)
          })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
