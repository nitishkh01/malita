import { Http } from '@/http'

const state = {
  familyCrops: [],
  familyFarms: [],
  farmerOrganizations: [],
  familyLivestock: [],
  familyPetAndPoultries: []
}

const getters = {
  familyCrops: state => state.familyCrops,
  familyFarms: state => state.familyFarms,
  farmerOrganizations: state => state.farmerOrganizations,
  familyLivestock: state => state.familyLivestock,
  familyPetAndPoultries: state => state.familyPetAndPoultries
}

const mutations = {
  setFamilyCrops: (state, familyCrops) => {
    state.familyCrops = familyCrops
  },
  setFamilyFarms: (state, familyFarms) => {
    state.familyFarms = familyFarms
  },
  setFarmerOrganizations: (state, farmerOrganizations) => {
    state.farmerOrganizations = farmerOrganizations
  },
  setFamilyLivestock: (state, familyLivestock) => {
    state.familyLivestock = familyLivestock
  },
  setFamilyPetAndPoultries: (state, familyPetAndPoultries) => {
    state.familyPetAndPoultries = familyPetAndPoultries
  }
}

const actions = {
  fetchFamilyCrops: ({ commit }, id) => {
    let url = `/agriculture/family-crops/${id}`

    return new Promise((resolve, reject) => {
      Http.get(url)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error.response)
          })
    })
  },
  fetchFamilyFarms: ({ commit }, id) => {
    let url = `/agriculture/family-farms/${id}`

    return new Promise((resolve, reject) => {
      Http.get(url)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error.response)
          })
    })
  },
  fetchFarmerOrganizations: ({ commit }, id) => {
    let url = `/agriculture/farmer-organizations/${id}`

    return new Promise((resolve, reject) => {
      Http.get(url)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error.response)
          })
    })
  },
  fetchFamilyLivestock: ({ commit }, id) => {
    let url = `/agriculture/family-livestocks/${id}`

    return new Promise((resolve, reject) => {
      Http.get(url)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error.response)
          })
    })
  },
  fetchFamilyPetAndPoultries: ({ commit }, id) => {
    let url = `/agriculture/family-pet-and-poultries/${id}`

    return new Promise((resolve, reject) => {
      Http.get(url)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error.response)
          })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
