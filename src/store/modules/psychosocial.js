import { Http } from '@/http'

const state = {
  psychosocial: [],
  patchPsychoSocial: []
}

const getters = {
  psychosocial: state => state.psychosocial,
  patchPsychoSocial: state => state.patchPsychoSocial
}

const mutations = {
  setPsychosocial: (state, psychosocial) => {
    state.psychosocial = psychosocial
  },
  setPatchPsychoSocial: (state, patchPsychoSocial) => {
    state.patchPsychoSocial = patchPsychoSocial
  }
}

const actions = {
  fetchPsychosocial: ({ commit }, id) => {
    let url = `/families/psychosocial/${id}`

    return new Promise((resolve, reject) => {
      Http.get(url)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error.response)
          })
    })
  },
  patchPsychoSocial: ({ commit }, payload) => {
    let url = `/families/psychosocial/${payload.id}/`
    return new Promise((resolve, reject) => {
      Http.patch(url, payload)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            console.log(error.response)
            reject(error.response)
          })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
