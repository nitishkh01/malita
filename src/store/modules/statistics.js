import querystring from 'querystring'

import { Http } from '@/http'

const state = {
  barangayPopulation: [],
  bloodType: [],
  disasterVulnerability: [],
  familyIncomes: [],
  genderPerEducationalLevel: [],
  governmentAid: [],
  gender: []
}

const getters = {
  barangayPopulation: state => state.barangayPopulation,
  bloodType: state => state.bloodType,
  disasterVulnerability: state => state.disasterVulnerability,
  familyIncomes: state => state.familyIncomes,
  genderPerEducationalLevel: state => state.genderPerEducationalLevel,
  governmentAid: state => state.governmentAid,
  gender: state => state.gender
}

const mutations = {
  setBarangayPopulation: (state, barangayPopulation) => {
    state.barangayPopulation = barangayPopulation
  },
  setBloodType: (state, bloodType) => {
    state.bloodType = bloodType
  },
  setDisasterVulnerability: (state, disasterVulnerability) => {
    state.disasterVulnerability = disasterVulnerability
  },
  setFamilyIncomes: (state, familyIncomes) => {
    state.familyIncomes = familyIncomes
  },
  setGenderPerEducationalLevel: (state, genderPerEducationalLevel) => {
    state.genderPerEducationalLevel = genderPerEducationalLevel
  },
  setGovernmentAid: (state, governmentAid) => {
    state.governmentAid = governmentAid
  },
  setGender: (state, gender) => {
    state.gender = gender
  }
}

const actions = {
  fetchBarangayPopulation: ({ commit }, filters) => {
    let url = `/graphs/barangay-population/`

    if (filters) {
      url = `${url}?${querystring.stringify(filters)}`
    }
    return new Promise((resolve, reject) => {
      Http.get(url)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error.response)
          })
    })
  },
  fetchBloodType: ({ commit }, filters) => {
    let url = `/graphs/blood-type/`

    if (filters) {
      url = `${url}?${querystring.stringify(filters)}`
    }
    return new Promise((resolve, reject) => {
      Http.get(url)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error.response)
          })
    })
  },
  fetchDisasterVulnerability: ({ commit }, filters) => {
    let url = `/graphs/disaster-risk/`

    if (filters) {
      url = `${url}?${querystring.stringify(filters)}`
    }
    return new Promise((resolve, reject) => {
      Http.get(url)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error.response)
          })
    })
  },
  fetchFamilyIncomes: ({ commit }, filters) => {
    let url = `/graphs/income/`

    if (filters) {
      url = `${url}?${querystring.stringify(filters)}`
    }
    return new Promise((resolve, reject) => {
      Http.get(url)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error.response)
          })
    })
  },
  fetchGenderPerEducationalLevel: ({ commit }, filters) => {
    let url = `/graphs/gender-education/`

    if (filters) {
      url = `${url}?${querystring.stringify(filters)}`
    }
    return new Promise((resolve, reject) => {
      Http.get(url)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error.response)
          })
    })
  },
  fetchGovernmentAid: ({ commit }, filters) => {
    let url = `/graphs/government-aid-received/`

    if (filters) {
      url = `${url}?${querystring.stringify(filters)}`
    }
    return new Promise((resolve, reject) => {
      Http.get(url)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error.response)
          })
    })
  },
  fetchGender: ({ commit }, filters) => {
    let url = `/graphs/gender/`

    if (filters) {
      url = `${url}?${querystring.stringify(filters)}`
    }
    return new Promise((resolve, reject) => {
      Http.get(url)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error.response)
          })
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
